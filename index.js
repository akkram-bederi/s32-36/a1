const express = require('express');
const mongoose = require('mongoose');
const dotenv=require(`dotenv`);
const cors=require(`cors`);
dotenv.config();


const app = express()	
const PORT = process.env.PORT || 3008;

//connect routes to index.js file
const userRoutes=require(`./Routes/userRoutes`);
const courseRoutes=require(`./Routes/courseRoutes`);

//Middlewares
app.use(express.json())
app.use(express.urlencoded({extended:true}))
app.use(cors())
//prevents blocking of request from client esp different domains

// Mongoose connection
mongoose.connect(process.env.MONGO_URL,{useNewUrlParser: true, useUnifiedTopology: true});

//DB connection notification
const db=mongoose.connection

db.on('error', console.error.bind(console, 'connection error:'));
db.once('open',() =>console.log(`Connected to Database`));

app.use(`/api/users`, userRoutes);
app.use(`/api/courses`, courseRoutes);

app.listen(PORT, ()=>console.log(`Server connected at port ${PORT}`))