const express = require(`express`);
const router = express.Router();

const {
    createCourse,
    getAllCourses,
    getCourse,
    updateCourse,
    courseArchive,
    courseUnarchive,
    activeCourses,
    updateCourse1
} = require (`./../controllers/courseControllers`)

const {verifyCourse, decodeCourse, verifyAdmin} = require(`./../auth`);

//CREATE A NEW COURSE
router.post(`/createCourse`, verifyAdmin, async(req,res)=>{
	try{
		createCourse(req.body).then(result => res.send(result))
	}catch(err){
		res.status(500).json(err)
	}
})	

//GET ALL COURSES

router.get(`/`, async(req,res)=>{
	try{
		getAllCourses().then(result => res.send(result))
	}catch(err){
		res.status(500).json(err)
	}
})

router.get(`/isActive`, async (req, res) => {
	try{
		await activeCourses().then(result => res.send(result))
	}catch(err){
		res.status(500).json(err)
	}
})

// //GET SPECIFIC COURSE
router.get(`/:courseId/`, async(req,res)=>{
	const id=req.params.courseId
	console.log(id)
	try{
		getCourse(id).then(result => res.send(result))
	}catch (err){
		res.status(500).json(err)
	}
})

//UPDATE A COURSE
router.patch(`/:courseid/updateCourse`, verifyAdmin, async(req,res)=>{
	const id =req.params.courseid
	try{
		updateCourse(id,req.body).then(result => res.send(result))
	}catch(err){
		res.status(500).json(err)
	}
})

//ARCHIVE
router.patch(`/:courseid/archive`, verifyAdmin, async(req,res)=>{
	const id =req.params.courseid
	try{
		await courseArchive(id).then(result => res.send(result))
	}catch(err){
		res.status(500).json(err)
	}
})

//UNARCHIVE
router.patch(`/:courseid/unarchive`, verifyAdmin, async(req,res)=>{
	const id =req.params.courseId
	try{
        await courseUnarchive(id).then(result => res.send(result))
    } catch(err){
        		res.status(500).json(err)
        	}
})


//GET ALL ACTIVE COURSE
// router.get(`/activecourses`, async(req,res)=>{
// 	try{
// 		await activeCourses().then(result=>res.send(result))
// 	} catch(err){
// 		res.status(500).json(err)
// 	}
// })

// router.put(`/isActive`, async (req, res) => {
// 	try{
// 		await activeCourses().then(result => res.send(result))
// 	}catch(err){
// 		res.status(500).json(err)
// 	}
// })

// router.patch(`/updateCourses`, async(req,res)=>{
// 	try{
// 		updateCourse1(req.body).then(result => res.send(result))
// 	}catch(err){
// 		res.status(500).json(err)
// 	}
// })





module.exports = router;
