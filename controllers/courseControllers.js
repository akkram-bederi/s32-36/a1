const Course = require (`./../models/course`)
const bcrypt = require('bcrypt')
const CryptoJS = require("crypto-js");
const jwt = require("jsonwebtoken");
const {createToken}=require(`./../auth`)

//CREATE A COURSE

module.exports.createCourse=async(reqBody) => {
	//return await .save()
	const {courseName,description,price,isOffered} =reqBody

	const newCourse= new Course ({
		courseName:courseName,
		description:description,
		price:price,
		isOffered:isOffered
	})

	return await newCourse.save().then(result =>{
		if(result){
			return true
		} else {
			if(result == null){
				return false
			}
		}
	})
}


// GET ALL COURSES
module.exports.getAllCourses = async () => {
    return await Course.find().then(result => result)
}


// RETRIEVE COURSE INFO
module.exports.getCourse = async (id) => {
    return await Course.findById(id).then((result, err) => {
        if(result){
            return result
        } else{
            if(result == null){
                return {message: `Course does not exist`}
            } else {
                return err
            }
        }
    })
}

// UPDATE COURSE 
module.exports.updateCourse = async (id, reqBody) => {
    return await Course.findByIdAndUpdate(id, {$set: reqBody}, {new: true}).then((result, err) => {
        console.log(result)
        if(result) {
            return result
        } else {
            return err
        }
    })
}

// ARCHIVE COURSE
module.exports.courseArchive = async (courseId) => {
	console.log(`hi`)
	return await Course.findOneAndUpdate(courseId, {$set: {isOffered: false}}, {new:true}).then((result, err) => result ? true : err)
}

// UNARCHIVE COURSE
module.exports.courseUnarchive = async (courseId) => {
	return await Course.findOneAndUpdate(courseId, {$set: {isOffered: true}}, {new:true}).then((result, err) => result ? true : err)
}

//ALL ACTIVE COURSE
module.exports.activeCourses = async () => {
    return await Course.find({isOffered: true}).then((result, err) => {
        if(result){
            return result
        } else {
            if(result == null){
                return `No active courses found`
            } else {
                return err
            }
        }
    })
}

module.exports.updateCourse1 = async (reqBody) => {
    return await Course.findOneAndUpdate({name:reqBody.name}, {$set: reqBody}, {new: true}).then((result, err) => {
        if(result) {
            return result
        } else {
            return err
        }
    })
}
